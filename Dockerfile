FROM rust:1-alpine

MAINTAINER Christian Mauduit "ufoot@ufoot.org"
LABEL package="fakelogs" version="0.1.10" description="fakelogs is a random log generator. It can be used for load testing of log parsers."

ADD ./ /wk/
WORKDIR /wk/
RUN cargo build --release
RUN cp /wk/target/release/fakelogs /

# Expose port 80 instead of 8080, running in a docker environment,
# there's usually no port conflict and it's rather the caller who
# will change the port, eg: docker run -p 8080:80 fakelogs
# This way, the image can be used, vanilla, in docker environments
# as a drop-in replacement for apache or nginx for testing purposes.

USER nobody
ENTRYPOINT ["/fakelogs"]
