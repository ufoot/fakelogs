extern crate chrono;
extern crate rand;

use crate::options;

use chrono::offset::Utc;
use chrono::DateTime;
use chrono::Duration;
use rand::Rng;
use std::cmp::max;
use std::io::Error;

/// Data used to define line content.
///
/// Random lines generation is a 2 step process, first generate a Line,
/// then print this line.
pub struct Line {
    /// IP address.
    pub ip_address: &'static str,
    /// User issuing the request.
    pub user: &'static str,
    /// Timestamp of the request.
    pub timestamp: DateTime<Utc>,
    /// Request method (GET, POST, ...)
    pub request_method: &'static str,
    /// URL section, can be empty.
    pub request_url_section: &'static str,
    /// 1st random char.
    pub request_url_r0: &'static str,
    /// 2nd random char.
    pub request_url_r1: &'static str,
    /// 3rd random char.
    pub request_url_r2: &'static str,
    /// 4th random char.
    pub request_url_r3: &'static str,
    /// The filename, eg: index.
    pub request_url_file: &'static str,
    /// The file extension, eg: html.
    pub request_url_ext: &'static str,
    /// The HTTP code, from 200 to 599.
    pub http_code: u16,
    /// The content size.
    pub content_size: i64,
    /// If true, consider the line is junk.
    pub junk: bool,
}

const IP_ADDRESSES: [&str; 4] = ["127.0.0.1", "192.168.0.1", "192.168.0.2", "192.168.0.3"];
const USERS: [&str; 6] = ["pim", "pim", "pim", "pam", "pam", "poum"];
const HTTP_CODES: [u16; 20] = [
    200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 302, 302, 302, 302, 302, 401, 403, 404, 404,
    500,
];
const REQUEST_METHODS: [&str; 5] = ["GET", "GET", "GET", "POST", "HEAD"];
const REQUEST_SECTIONS: [&str; 20] = [
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/yolo",
    "/foo/bar",
    "/foo/bar",
    "/foo/bar",
    "/bar/foo",
    "/bar/foo",
    "/bar/foo",
    "",
    "",
    "",
    "/pizzapino",
];
const REQUEST_RS: [&str; 62] = [
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
    "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
    "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9",
];
const REQUEST_FILES: [&str; 3] = ["index", "hidden", "secret"];
const REQUEST_EXTS: [&str; 4] = ["html", "jpg", "css", "txt"];
const MIN_CONTENT_SIZE: i64 = 100;
const MAX_CONTENT_SIZE: i64 = 19900;
const JUNK_RATIO: f64 = 0.001;
const TIME_SKEW_SECONDS: i64 = 1800;

fn card_char(cfg: &options::Config, mut rng: impl Rng) -> &'static str {
    if cfg.high_card {
        REQUEST_RS[rng.gen_range(0, REQUEST_RS.len())]
    } else {
        "x"
    }
}

fn junk_flag(cfg: &options::Config, mut rng: impl Rng) -> bool {
    cfg.junk && rng.gen_bool(JUNK_RATIO)
}

fn now_timestamp(cfg: &options::Config, mut rng: impl Rng) -> DateTime<Utc> {
    let delta_jitter = Duration::seconds(if cfg.time_jitter {
        (max(rng.gen_range(0, 10), 8) - 8) * (rng.gen_range(0, 120) - 60)
    } else {
        0
    });
    let delta_skew = Duration::seconds(if cfg.time_skew { TIME_SKEW_SECONDS } else { 0 });
    Utc::now() + delta_jitter + delta_skew
}

impl Line {
    /// Generate a random line.
    ///
    /// ```
    /// use fakelogs::{generator, options};
    ///
    /// let l = generator::Line::random(&options::Config::default());
    /// assert!(l.http_code >= 200);
    /// assert!(l.http_code <= 599);
    /// ```
    pub fn random(cfg: &options::Config) -> Line {
        let mut rng = rand::thread_rng();
        let l = Line {
            ip_address: IP_ADDRESSES[rng.gen_range(0, IP_ADDRESSES.len())],
            user: USERS[rng.gen_range(0, USERS.len())],
            timestamp: now_timestamp(cfg, rng),
            request_method: REQUEST_METHODS[rng.gen_range(0, REQUEST_METHODS.len())],
            request_url_section: REQUEST_SECTIONS[rng.gen_range(0, REQUEST_SECTIONS.len())],
            request_url_r0: card_char(cfg, rng),
            request_url_r1: card_char(cfg, rng),
            request_url_r2: card_char(cfg, rng),
            request_url_r3: card_char(cfg, rng),
            request_url_file: REQUEST_FILES[rng.gen_range(0, REQUEST_FILES.len())],
            request_url_ext: REQUEST_EXTS[rng.gen_range(0, REQUEST_EXTS.len())],
            http_code: HTTP_CODES[rng.gen_range(0, HTTP_CODES.len())],
            content_size: rng.gen_range(MIN_CONTENT_SIZE, MAX_CONTENT_SIZE),
            junk: junk_flag(cfg, rng),
        };
        return l;
    }
}

impl Line {
    fn sep(&self) -> &'static str {
        if self.request_url_section.len() > 0 {
            "-"
        } else {
            "/"
        }
    }
}

fn write_junk(w: &mut (dyn std::io::Write + std::marker::Sync)) -> Result<usize, Error> {
    return w.write("Your attention please, this is a hack!\n".as_bytes());
}

/// Write a header for a common log format. This typically outputs a comment
/// line with the start time. Comment lines do not exist in CLF, so this is
/// technically an invalid line.
pub fn write_common_log_header(w: &mut (dyn std::io::Write)) -> Result<usize, Error> {
    let dt: DateTime<Utc> = Utc::now();
    let ts = dt.format("%d/%b/%Y:%H:%M:%S %z");
    let content = format!(
        "# [{}] start fakelogs version {}\n",
        ts,
        options::VERSION.unwrap_or("unknown"),
    );
    w.write(content.as_bytes())
}

/// Write a header for CSV format. This helps parsing CSV as it gives a name
/// to every column, so afterwards it's easy to just call the data by their
/// column name. Python pandas does this, for example.
pub fn write_csv_header(w: &mut (dyn std::io::Write)) -> Result<usize, Error> {
    let content =
        "\"remotehost\",\"rfc931\",\"authuser\",\"date\",\"request\",\"status\",\"bytes\"\n";
    w.write(content.as_bytes())
}

impl Line {
    /// Write a line in common log format.
    ///
    /// ```
    /// use std::io;
    /// use fakelogs::{generator, options};
    ///
    /// let l = generator::Line::random(&options::Config::default());
    /// l.write_common_log(&mut io::stdout()).unwrap();
    ///
    /// // Common log format examples:
    /// // 127.0.0.1 - james [09/May/2018:16:00:39 +0000] "GET /report HTTP/1.0" 200 123
    /// // 127.0.0.1 - jill [09/May/2018:16:00:41 +0000] "GET /api/user HTTP/1.0" 200 234
    /// // 127.0.0.1 - frank [09/May/2018:16:00:42 +0000] "POST /api/user HTTP/1.0" 200 34
    /// // 127.0.0.1 - mary [09/May/2018:16:00:42 +0000] "POST /api/user HTTP/1.0" 503 12
    /// ```
    pub fn write_common_log(
        &self,
        w: &mut (dyn std::io::Write + std::marker::Sync),
    ) -> Result<usize, Error> {
        if self.junk {
            return write_junk(w);
        }

        let dt: DateTime<Utc> = self.timestamp;
        let ts = dt.format("%d/%b/%Y:%H:%M:%S %z");

        let content = format!(
            "{} - {} [{}] \"{} {}/{}{}{}{}{}{}.{} HTTP/1.0\" {} {}\n",
            self.ip_address,
            self.user,
            ts,
            self.request_method,
            self.request_url_section,
            self.request_url_r0,
            self.request_url_r1,
            self.request_url_r2,
            self.request_url_r3,
            self.sep(),
            self.request_url_file,
            self.request_url_ext,
            self.http_code,
            self.content_size
        );
        return w.write(content.as_bytes());
    }

    /// Write a line in CSV format.
    ///
    /// ```
    /// use std::io;
    /// use fakelogs::{generator, options};
    ///
    /// let l = generator::Line::random(&options::Config::default());
    /// l.write_csv(&mut io::stdout()).unwrap();
    ///
    /// // CSV format example:
    /// // "10.0.0.4","-","apache",1549573860,"GET /api/user HTTP/1.0",200,1234
    /// ```
    pub fn write_csv(
        &self,
        w: &mut (dyn std::io::Write + std::marker::Sync),
    ) -> Result<usize, Error> {
        if self.junk {
            return write_junk(w);
        }

        let dt: DateTime<Utc> = self.timestamp;
        let ts = dt.timestamp();
        let content = format!(
            "\"{}\",\"-\",\"{}\",{},\"{} {}/{}{}{}{}{}{}.{} HTTP/1.0\",{},{}\n",
            self.ip_address,
            self.user,
            ts,
            self.request_method,
            self.request_url_section,
            self.request_url_r0,
            self.request_url_r1,
            self.request_url_r2,
            self.request_url_r3,
            self.sep(),
            self.request_url_file,
            self.request_url_ext,
            self.http_code,
            self.content_size
        );
        return w.write(content.as_bytes());
    }
}

#[cfg(test)]
mod tests {
    use chrono::offset::Utc;
    use chrono::DateTime;

    #[test]
    fn test_random_line() {
        let l = super::Line::random(&super::options::Config::default());
        assert!(l.ip_address.len() > 0);
        assert!(l.user.len() > 0);
        let dt: DateTime<Utc> = l.timestamp;
        let ts = dt.timestamp();
        assert!(ts > 1500000000);
        assert!(ts < 5000000000);
        match l.request_method {
            "GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "CONNECT" | "OPTIONS" | "PATCH" => (),
            _ => panic!(l.request_method),
        }
        assert_eq!(1, l.request_url_r0.len());
        assert_eq!(1, l.request_url_r1.len());
        assert_eq!(1, l.request_url_r2.len());
        assert_eq!(1, l.request_url_r3.len());
        assert!(2 <= l.request_url_ext.len());
        assert!(4 >= l.request_url_ext.len());
        assert!(100 <= l.http_code);
        assert!(599 >= l.http_code);
        assert!(0 < l.content_size);
        assert!(1000000000 >= l.content_size);
    }

    #[test]
    fn test_write_common_log() {
        let dt: DateTime<Utc> = DateTime::<Utc>::from_utc(
            DateTime::parse_from_rfc3339("1996-12-19T16:39:57-08:00")
                .unwrap()
                .naive_utc(),
            Utc,
        );
        let mut buf: Vec<u8> = Vec::new();

        let l: super::Line = super::Line {
            ip_address: "127.0.0.1",
            user: "foo",
            timestamp: dt,
            request_method: "GET",
            request_url_section: "/whooops",
            request_url_r0: "a",
            request_url_r1: "2",
            request_url_r2: "c",
            request_url_r3: "4",
            request_url_file: "hack-me",
            request_url_ext: "bin",
            http_code: 200,
            content_size: 421,
            junk: false,
        };

        let l: usize = l.write_common_log(&mut buf).unwrap();
        assert_eq!(94, l);
        let v: &str = std::str::from_utf8(&buf).unwrap();
        assert_eq!(
        "127.0.0.1 - foo [20/Dec/1996:00:39:57 +0000] \"GET /whooops/a2c4-hack-me.bin HTTP/1.0\" 200 421\n",
        v
    );
    }

    #[test]
    fn test_write_csv() {
        let dt: DateTime<Utc> = DateTime::<Utc>::from_utc(
            DateTime::parse_from_rfc3339("1996-12-19T16:39:57-08:00")
                .unwrap()
                .naive_utc(),
            Utc,
        );
        let mut buf: Vec<u8> = Vec::new();

        let l: super::Line = super::Line {
            ip_address: "127.0.0.1",
            user: "foo",
            timestamp: dt,
            request_method: "GET",
            request_url_section: "/whooops",
            request_url_r0: "a",
            request_url_r1: "2",
            request_url_r2: "c",
            request_url_r3: "4",
            request_url_file: "root-me",
            request_url_ext: "bin",
            http_code: 200,
            content_size: 421,
            junk: false,
        };

        let l: usize = l.write_csv(&mut buf).unwrap();
        assert_eq!(81, l);
        let v: &str = std::str::from_utf8(&buf).unwrap();
        assert_eq!(
        "\"127.0.0.1\",\"-\",\"foo\",851042397,\"GET /whooops/a2c4-root-me.bin HTTP/1.0\",200,421\n",
        v
    );
    }

    #[test]
    fn test_write_junk() {
        let dt: DateTime<Utc> = DateTime::<Utc>::from_utc(
            DateTime::parse_from_rfc3339("1996-12-19T16:39:57-08:00")
                .unwrap()
                .naive_utc(),
            Utc,
        );
        let mut buf: Vec<u8> = Vec::new();

        let l: super::Line = super::Line {
            ip_address: "127.0.0.1",
            user: "foo",
            timestamp: dt,
            request_method: "GET",
            request_url_section: "/whooops",
            request_url_r0: "a",
            request_url_r1: "2",
            request_url_r2: "c",
            request_url_r3: "4",
            request_url_file: "root-me",
            request_url_ext: "bin",
            http_code: 200,
            content_size: 421,
            junk: true,
        };

        let l: usize = l.write_common_log(&mut buf).unwrap();
        assert_eq!(39, l);
        let v: &str = std::str::from_utf8(&buf).unwrap();
        assert_eq!("Your attention please, this is a hack!\n", v);
    }
}
