//! [Fakelogs](https://gitlab.com/ufoot/fakelogs) is a random log generator.
//! It can be used for load testing of log parsers.
//!
//! It is written in [Rust](https://www.rust-lang.org/)
//! and is mostly a toy project to ramp up on the language.
//! It might however be useful. Use at your own risk.
//!
//! ![Fakelogs icon](https://gitlab.com/ufoot/fakelogs/raw/master/fakelogs.png)

pub mod generator;
pub mod options;

extern crate chrono;
extern crate rand;
extern crate ticker;

use rand::Rng;
use std::borrow::BorrowMut;
use std::io;
use std::io::Error;
use std::time::Duration;
use ticker::Ticker;

fn header(cfg: &options::Config) -> Result<(), std::io::Error> {
    if !cfg.header {
        return Ok(());
    }
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    match if cfg.csv_output {
        generator::write_csv_header(&mut handle.borrow_mut())
    } else {
        generator::write_common_log_header(&mut handle.borrow_mut())
    } {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}

fn chunk(i: impl Iterator<Item = i64>, cfg: &options::Config) -> Result<(), std::io::Error> {
    for _ in i {
        let l = generator::Line::random(&cfg);
        let stdout = io::stdout();
        let mut handle = stdout.lock();
        let r: Result<usize, Error>;
        if cfg.csv_output {
            r = l.write_csv(handle.borrow_mut());
        } else {
            r = l.write_common_log(handle.borrow_mut());
        }
        r?;
    }
    Ok(())
}

///  Run the generator with the given options.
///
/// ```no_run
/// use fakelogs::{options, run};
///
/// run(&options::Config::parse());
/// ```
pub fn run(cfg: &options::Config) {
    const CHUNK_SECS: i64 = 5;
    const BURST_FREQ: f64 = 0.4;

    let mut freq: f64 = cfg.lines_per_second as f64;
    if freq < 1.0 {
        freq = 1.0;
    }
    let b_freq = if cfg.burst { BURST_FREQ } else { 1.0 };
    let mut d_micros = 1000000.0 * b_freq / freq;
    if d_micros < 1.0 {
        d_micros = 1.0;
    }
    let n = CHUNK_SECS * 1000000 / (d_micros as i64);
    let d = Duration::from_micros(d_micros as u64);

    match header(cfg) {
        Ok(()) => {}
        Err(e) => {
            eprintln!("{}", e);
            return;
        }
    }

    loop {
        let mut rng = rand::thread_rng();
        match if (!cfg.burst) || rng.gen_bool(BURST_FREQ) {
            chunk(Ticker::new(0..n, d).into_iter(), &cfg)
        } else {
            chunk(
                Ticker::new(0..CHUNK_SECS, Duration::from_secs(1)).into_iter(),
                &cfg,
            )
        } {
            Ok(()) => {}
            Err(e) => {
                eprintln!("{}", e);
                break;
            }
        }
    }
}
