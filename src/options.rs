use std::env;
use std::process::exit;

/// Configuration struct, all program behavior is defined in those fields.
pub struct Config {
    /// Whether the output should be CSV of Common Log format.
    pub csv_output: bool,
    /// Number of lines per second when generating high load.
    pub lines_per_second: u32,
    /// If true, generate random high-cardinality data.
    pub high_card: bool,
    /// If true, introduce some time skew in log lines timestamps, pretending they are in the past.
    pub time_skew: bool,
    /// If true, introduce some time jittering in log lines timestamps.
    pub time_jitter: bool,
    /// If true, generate a header lien when starting the program.
    pub header: bool,
    /// If true, generate a junk line, syntaxically incorrect, from time to time.
    pub junk: bool,
    /// If true, alternate between burst periods with high load an calm periods with low load.
    pub burst: bool,
}

/// Version of the program, eg: 0.1.2-deadbeef
pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const DEFAULT_LINES_PER_SECOND: u32 = 1000;
const MAX_LINES_PER_SECOND: u32 = 1000000;

impl Config {
    /// Parses the command line arguments.
    pub fn parse() -> Config {
        let mut c = Config::default();
        let mut first: bool = true;

        for arg in env::args() {
            if first {
                first = false;
                continue;
            }
            match arg.as_str() {
                "-c" | "--csv" => {
                    c.csv_output = true;
                    continue;
                }
                "--no-high-card" => {
                    c.high_card = false;
                    continue;
                }
                "--no-time-skew" => {
                    c.time_skew = false;
                    continue;
                }
                "--no-time-jitter" => {
                    c.time_jitter = false;
                    continue;
                }
                "--no-header" => {
                    c.header = false;
                    continue;
                }
                "--no-junk" => {
                    c.junk = false;
                    continue;
                }
                "--no-burst" => {
                    c.burst = false;
                    continue;
                }
                "-h" | "--help" => {
                    help();
                    exit(0);
                }
                "-v" | "--version" => {
                    version();
                    exit(0);
                }
                _ => (),
            }
            if arg.len() > 1 && arg[0..1] == *"-" {
                match arg[1..].parse::<u32>() {
                    Ok(n) => {
                        if n > 0 && n <= MAX_LINES_PER_SECOND {
                            c.lines_per_second = n;
                        } else {
                            println!("invalid number of lines per second: {}", n);
                            println!();
                            usage();
                            exit(1);
                        }
                        continue;
                    }
                    Err(e) => {
                        println!("error parsing lines per second \"{}\": {}", arg, e);
                        println!();
                        usage();
                        exit(1);
                    }
                }
            }
            println!("invalid arg: \"{}\"", arg);
            println!();
            usage();
            exit(1);
        }

        return c;
    }
}

impl std::default::Default for Config {
    /// Return a default config.
    ///
    /// ```
    /// use fakelogs::options;
    ///
    /// let c = options::Config::default();
    /// assert_eq!(1000, c.lines_per_second);
    /// ```
    fn default() -> Self {
        Config {
            csv_output: false,
            lines_per_second: DEFAULT_LINES_PER_SECOND,
            high_card: true,
            time_skew: true,
            time_jitter: true,
            header: true,
            junk: true,
            burst: true,
        }
    }
}

fn help() {
    copyright();
    println!();
    usage();
}

fn version() {
    println!("fakelogs-{}", VERSION.unwrap_or("unknown"));
}

fn copyright() {
    println!("fakelogs is licensed under the MIT license");
    println!("Copyright (C) 2020 Christian Mauduit <ufoot@ufoot.org>");
    println!();
    println!("https://gitlab.com/ufoot/fakelogs");
}

fn usage() {
    println!("usage: fakelogs [-c] [-h] [-v] [-<lines-per-second>]");
    println!("example: fakelogs -100");
    println!();
    println!("options:");
    println!("-c,--csv: switch output to CSV");
    println!(
        "-10: output 10 lines per second (any number up to {}, default {})",
        MAX_LINES_PER_SECOND, DEFAULT_LINES_PER_SECOND
    );
    println!("-h,--help: display help");
    println!("-v,--version: show version");
    println!("--no-high-card: disable high cardinality");
    println!("--no-time-skew: disable time skewing");
    println!("--no-time-jitter: disable time jittering");
    println!("--no-header: skip the header line");
    println!("--no-junk: no random junk lines");
    println!("--no-burst: no random burst behavior");
}
