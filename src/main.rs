use fakelogs::options;
use fakelogs::run;

fn main() {
    let cfg = options::Config::parse();
    run(&cfg);
}
